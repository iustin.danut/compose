QuantumCore - Compose
=====================
This project contains example files used for hosting a QuantumCore server using
Docker Compose.

Prerequirements
---------------
- Any supported operating system by Docker.
- Docker

Get started
-----------
As we cannot provide all needed files you have to insert some files in the `data/` folder.

Check out the QuantumCore documentation to read what files are currently needed.
[Documentaton](https://docs.quantum-core.io/running.html#additional-files)

Commands
--------
To start the server you only have to execute the following command:
```
docker-compose up -d
```

To shutdown the server execute the following command: 
```
docker-compose down
```

If you want to upgrade QuantumCore and it's dependencies (redis, mariadb) execute the following command:
```
docker-compose pull
```

To expect the server output you can execute the following command
```
docker-compose logs auth
```
or
```
docker-compose logs game
```